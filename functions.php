<?php
/**
 * Execute the given command by displaying console output live to the user.
 *  @param  string  cmd          :  command to be executed
 *  @return array   exit_status  :  exit status of the executed command
 *                  output       :  console output of the executed command
 */
function liveExecuteCommand($cmd)
{

    while (@ ob_end_flush()); // end all output buffers if any

    $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

    $live_output     = "";
    $complete_output = "";

    while (!feof($proc))
    {
        $live_output     = fread($proc, 4096);
        $complete_output = $complete_output . $live_output;
        echo "$live_output";
        @ flush();
    }

    pclose($proc);

    // get exit status
    preg_match('/[0-9]+$/', $complete_output, $matches);

    // return exit status and intended output
    return array (
                    'exit_status'  => intval($matches[0]),
                    'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
                 );
}


function parseArtists($artistsArray) {
    $keys = count($artistsArray)-1;
    $artists = null;
    foreach ($artistsArray as $key=>$artist) {
        $artists = $artists . $artist["name"];
        if($key != $keys) {
            $artists = $artists . ", ";
        }
    }
    return $artists;
}

function releaseYear($releaseDate) {
    $releaseYear = substr($releaseDate, 0, 4);
    return $releaseYear;
}

function retrieveCover($coverString, $size) { // Possible size values: 80, 160, 320, 640, 1280
    $cover = "<img src=\"https://resources.tidal.com/images/". str_replace("-", "/", $coverString) ."/". $size ."x". $size .".jpg\"/>";
    return $cover;
}

function qualitySelector($quality) {
    $qualityList = [
        "Master",
        "HiFi",
        "High",
        "Normal",
        "Opus160k",
        "Opus128k",
        "Opus96k",
        "Opus64k",
        "Lame320k",
        "Lame256k",
        "Lame192k",
        "Lame128k"
    ];
    if(!in_array($_COOKIE["quality"], $qualityList)) {
        $quality = "HiFi";
    }

    return $quality;
}
