<?php
require_once("functions.php");

// Get Tidal's JSON response
$search_term = str_replace(" ", "+", htmlspecialchars($_REQUEST["q"]));
$json = file_get_contents("https://webapi.tidal.com/v1/search/all?albumPrices=true&countryCode=ES&prices=false&query=$search_term&token=zQi8ku7ogyrE05fg&types=TRACKS,ALBUMS");
$result = json_decode($json, true);

# Only for debugging!
#print("<pre>".print_r($result,true)."</pre>");

// Determine if topHit is an album or a single track
if(isset($result["topHit"])) { // If any hit is found then save the values
    if(isset($result["topHit"]["value"]["album"]["cover"])) {
        // Single track
        $topHitCover = retrieveCover($result["topHit"]["value"]["album"]["cover"], "320");
        $topHitInfo = gmdate("i:s", $result["topHit"]["value"]["duration"]) . " min";
        $topHitType = "track";
    } else {
        // Album
        $topHitCover = retrieveCover($result["topHit"]["value"]["cover"], "320");
        $topHitInfo =  $result["topHit"]["value"]["numberOfTracks"] . " tracks (" . gmdate("i:s", $result["topHit"]["value"]["duration"]) . " min)";
        $topHitType = "album";
    }
    $topHitTitle = $result["topHit"]["value"]["title"];
} else {
    $topHitTitle = "Nothing found, please try to search something else.";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <title>Search results - Tidal Media Web Downloader</title>

	<style>
	@media (max-width : 768px) {
		h3 {
			font-size: 1.5rem !important;
		}
		div#details {
			text-align: center !important;
		}
	}

    #topCover:hover {
        color: yellow;
    }
    </style>

</head>
<body>
	<main class="container">
        <!-- Top Hit Start -->
        <div class="p-5 mb-4 bg-light rounded-3" id="topHit"  style="padding-top:0px !important; padding-bottom:0px !important;">
            <div class="container-fluid py-5 row">
                <div class="col-md-4" id="topCover">
                    <?php 
                        echo $topHitCover;
                    ?>
                </div>
                <div class="col-md-8">
                    <h1 class="display-5 fw-bold"><?php echo $topHitTitle; ?></h1>
                    <p class="col-md-8 fs-4">
                        <?php
                            echo parseArtists($result["topHit"]["value"]["artists"]);
                            echo "<p class=\"small\">" . $topHitInfo . "</p>";
                        ?>
                    </p>
                    <p class="small"><?php echo releaseYear($item["streamStartDate"]); ?></p>
                    <a class="btn btn-primary btn-lg" href="../handle.php?id=<?php echo $result["topHit"]["value"]["id"];?>&type=<?php echo $topHitType;?>&quality=<?php echo qualitySelector($_COOKIE["quality"]);?>" role="button">Select</a>
                </div>
            </div>
        </div>
        <!-- Top Hit End -->

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#tracks">Tracks</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#albums">Albums</a>
            </li>
        </ul>

        <!-- Tracks Table Start -->
        <div class="tab-content">
            <div id="tracks" class="tab-pane container active">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">Cover</th>
                        <th scope="col">Title</th>
                        <th>Length</th>
                        <th scope="col">Release date</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                if($result["tracks"]["totalNumberOfItems"] > 0) {
                                    foreach($result["tracks"]["items"] as $item) {
                                        echo "<tr>";
                                            echo "<th scope=\"row\">". retrieveCover($item["album"]["cover"], "80") ."</th>";
                                            echo "<td><strong>$item[title]</strong><br>". parseArtists($item["artists"]) ."</td>";
                                            echo "<td>" . gmdate("i:s", $item[duration]) . " min</td>";
                                            echo "<td>". releaseYear($item["streamStartDate"]) ."</td>";
                                            echo "<td>
                                            <div class=\"btn-group dropdown dropdown-center\">
                                                <a class=\"btn btn-primary\" href=\"../handle.php?id=$item[id]&type=track&quality=". qualitySelector($_COOKIE["quality"]) . "\" role=\"button\">Select</a>
                                                <button type=\"button\" class=\"btn btn-primary dropdown-toggle dropdown-toggle-split\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">
                                                    <span class=\"visually-hidden\">Toggle Dropdown</span>
                                                </button>
                                                <ul class=\"dropdown-menu\">
                                                    <li><a class=\"dropdown-item\" href=\"$item[url]\">View on Tidal</a></li>
                                                </ul>
                                            </div>
                                            </td>"; 
                                        echo "</tr>";
                                    }
                            
                                } else {
                                    echo "<th scope=\"row\">No tracks found.</th>";
                                }
                        ?>
                    </tbody>
                </table>
            </div>
        <!-- Tracks Table End -->

        <!-- Albums Table Start -->
            <div id="albums" class="tab-pane container fade">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">Cover</th>
                        <th scope="col">Title</th>
                        <th>Number of tracks</th>
                        <th scope="col">Release date</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                if($result["albums"]["totalNumberOfItems"] > 0) {
                                    foreach($result["albums"]["items"] as $item) {
                                        echo "<tr>";
                                            echo "<th scope=\"row\">". retrieveCover($item["cover"], "80") ."</th>";
                                            echo "<td><strong>$item[title]</strong><br>". parseArtists($item["artists"]) ."</td>";
                                            echo "<td>". $item["numberOfTracks"] . " tracks (" . gmdate("i:s", $item["duration"]) . " min)</td>";
                                            echo "<td>". releaseYear($item["streamStartDate"]) ."</td>";
                                            echo "<td>
                                            <div class=\"btn-group dropdown dropdown-center\">
                                                <a class=\"btn btn-primary\" href=\"../handle.php?id=$item[id]&type=album&quality=". qualitySelector($_COOKIE["quality"]) . "\" role=\"button\">Select</a>
                                                <button type=\"button\" class=\"btn btn-primary dropdown-toggle dropdown-toggle-split\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">
                                                    <span class=\"visually-hidden\">Toggle Dropdown</span>
                                                </button>
                                                <ul class=\"dropdown-menu\">
                                                    <li><a class=\"dropdown-item\" href=\"$item[url]\">View on Tidal</a></li>
                                                </ul>
                                            </div>
                                            </td>"; 
                                        echo "</tr>";
                                    }
                            
                                } else {
                                    echo "<th scope=\"row\">No albums found.</th>";
                                }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Albums Table End -->

        
    </main>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
